## Hello My Name Is… Privacy Policy

Hi! This app does not collect your data. I do not have access to any of it. The app doesn’t even have internet access. Your data is safe.
