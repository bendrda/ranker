package me.drda.ben.ranker

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.activityViewModels
import androidx.preference.PreferenceManager
import de.gesundkrank.jskills.trueskill.TwoTeamTrueSkillCalculator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import java.util.*
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random

const val ANIMATION_DURATION = 200L
const val STARTING_ALPHA = 0.75f
const val STARTING_SCALE = 0.9f

class ContestFragment : DrdaFragment(), View.OnClickListener, CoroutineScope {

    private val job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private var itemList: ItemList? = null
    private val itemListId by lazy {
        ContestFragmentArgs.fromBundle(arguments ?: Bundle.EMPTY).itemListId
    }
    private val itemListModel by activityViewModels<ItemListModel>()
    private val titleModel by activityViewModels<TitleModel>()
    private val contestViewCache = mutableMapOf<Name, View>()
    private val unboundViewCache = LinkedList<View>()
    private var inFlight = true
    private var nameOne: Name? = null
    private var nameTwo: Name? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_contest, container, false)

        itemListModel.getItemList(itemListId)
            .observe(viewLifecycleOwner) { list ->
                itemList = list
                pickFight()
            }

        return v
    }

    override fun onStart() {
        super.onStart()
        titleModel.title.value = getString(R.string.contest)
    }

    override fun onClick(v: View) {
        if (!inFlight) {
            inFlight = true
            when (v.id) {
                R.id.dropButton -> drop(v.getTag(R.id.tag_name) as Name)
                R.id.pickButton -> pick(v.getTag(R.id.tag_name) as Name)
            }
        }
    }

    private fun drop(name: Name) {
        val nameOneView = nameOne?.let { viewForName(it) }
        val nameTwoView = nameTwo?.let { viewForName(it) }

        if (nameOneView != null && nameTwoView != null) {
            dropName(name)
            contestViewCache.remove(nameOne!!)
            contestViewCache.remove(nameTwo!!)
            dropOut(nameOneView, nameTwoView, nameOne == name)
        }
    }

    private fun pick(winner: Name) {
        val nameOneView = nameOne?.let { viewForName(it) }
        val nameTwoView = nameTwo?.let { viewForName(it) }
        if (nameOneView != null && nameTwoView != null) {
            val loser = if (nameOne!! == winner) {
                nameTwo!!
            } else {
                nameOne!!
            }
            compete(winner, loser)
            contestViewCache.remove(nameOne!!)
            contestViewCache.remove(nameTwo!!)
            animateOut(nameOneView, nameTwoView, nameOne == winner)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun pickFight() {
        itemList?.let { itemList ->
            (view as? ConstraintLayout)?.let { constraintLayout ->
                nameOne = itemList.findBestContestant()
                nameTwo = itemList.findClosest(nameOne!!)

                val newOneView = viewForName(nameOne!!, forceNew = true)
                val newTwoView = viewForName(nameTwo!!, forceNew = true)

                constraintLayout.addView(newOneView, 0)
                constraintLayout.addView(newTwoView, 0)

                val constraintSet = ConstraintSet()
                constraintSet.clone(constraintLayout)
                constraintSet.createVerticalChain(
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.BOTTOM,
                    intArrayOf(newOneView.id, newTwoView.id),
                    null,
                    ConstraintSet.CHAIN_SPREAD
                )
                constraintSet.setMargin(
                    newOneView.id,
                    ConstraintSet.BOTTOM,
                    resources.getDimensionPixelSize(R.dimen.padding)
                )
                constraintSet.setMargin(
                    newTwoView.id,
                    ConstraintSet.TOP,
                    resources.getDimensionPixelSize(R.dimen.padding)
                )
                constraintSet.connect(
                    newOneView.id,
                    ConstraintSet.START,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.START
                )
                constraintSet.connect(
                    newTwoView.id,
                    ConstraintSet.START,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.START
                )
                constraintSet.connect(
                    newOneView.id,
                    ConstraintSet.END,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.END
                )
                constraintSet.connect(
                    newTwoView.id,
                    ConstraintSet.END,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.END
                )
                constraintSet.constrainHeight(newOneView.id, ConstraintSet.MATCH_CONSTRAINT_SPREAD)
                constraintSet.constrainHeight(newTwoView.id, ConstraintSet.MATCH_CONSTRAINT_SPREAD)
                constraintSet.constrainWidth(newOneView.id, ConstraintSet.MATCH_CONSTRAINT)
                constraintSet.constrainWidth(newTwoView.id, ConstraintSet.MATCH_CONSTRAINT)
                constraintSet.applyTo(constraintLayout)
                animateIn(newOneView, newTwoView)
            }
        }
    }

    private fun animateIn(viewOne: View, viewTwo: View) {
        animateViewIn(viewOne)
        animateViewIn(viewTwo)
    }

    private fun animateViewIn(v: View) {
        v.alpha = STARTING_ALPHA
        v.scaleX = STARTING_SCALE
        v.scaleY = STARTING_SCALE
        v.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f)
            .setInterpolator(AccelerateDecelerateInterpolator()).setDuration(ANIMATION_DURATION)
            .setStartDelay(ANIMATION_DURATION + 50L)
            .withStartAction {
                drLogD("ANIM", "start action animate in ${v.id}")
            }
            .withEndAction {
                drLogD("ANIM", "end action animate in ${v.id}")
                inFlight = false
            }
            .start()
    }

    private fun animateOut(viewOne: View, viewTwo: View, viewOneIsWinner: Boolean) {
        val constraintLayout = view as ViewGroup
        animateViewOut(viewOne, constraintLayout, viewOneIsWinner)
        animateViewOut(viewTwo, constraintLayout, !viewOneIsWinner)
    }

    private fun animateViewOut(
        view: View,
        constraintLayout: ViewGroup,
        isWinner: Boolean
    ) {
        view.animate().translationX(
            constraintLayout.measuredWidth * if (isWinner) {
                1f
            } else {
                -1f
            }
        ).translationZBy(1f).setDuration(ANIMATION_DURATION).setStartDelay(0L)
            .setInterpolator(AccelerateInterpolator()).withStartAction {
                drLogD("ANIM", "start action animate out ${view.id}")
            }.withEndAction(resetNameViewEndAction(view)).start()
        drLogD("ANIM", "animate out trigger ${view.id}")
    }

    private fun dropOut(viewOne: View, viewTwo: View, viewOneIsDropped: Boolean) {
        val constraintLayout = view as ViewGroup
        dropViewOut(viewOne, constraintLayout, viewOneIsDropped)
        dropViewOut(viewTwo, constraintLayout, !viewOneIsDropped)
    }

    private fun dropViewOut(
        v: View,
        parent: ViewGroup,
        isDropped: Boolean
    ) =
        if (isDropped) {
            v.animate()
                .translationZ(2f)
                .scaleX(1.05f)
                .scaleY(1.05f)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .setDuration(50L)
                .setStartDelay(0L)
                .withEndAction {
                    v.animate()
                        .translationY(parent.measuredHeight.toFloat() + v.measuredWidth.toFloat())
                        .setDuration(ANIMATION_DURATION - 50L)
                        .setInterpolator(AccelerateInterpolator())
                        .rotation(
                            Random.nextDouble(25.0, 50.0).toFloat() * if (Random.nextBoolean()) {
                                -1f
                            } else {
                                1f
                            }
                        )
                        .setStartDelay(0L)
                        .withEndAction(resetNameViewEndAction(v))
                        .start()
                }.start()

        } else {
            v.animate()
                .scaleX(STARTING_SCALE)
                .scaleY(STARTING_SCALE)
                .alpha(STARTING_ALPHA)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .withEndAction(resetNameViewEndAction(v))
                .setStartDelay(0L)
                .setDuration(ANIMATION_DURATION)
                .start()
        }


    private fun compete(winner: Name, loser: Name) {
        drLogD("Ratings", "BEFORE: $winner v $loser")
        val ratings = TwoTeamTrueSkillCalculator().calculateNewRatings(
            GAME_INFO,
            listOf(winner.team, loser.team),
            1,
            2
        )
        val newWinner = winner.copy(rating = ratings[winner.player] ?: winner.rating)
        val newLoser = loser.copy(rating = ratings[loser.player] ?: loser.rating)

        itemListModel.updateNames(this, newWinner, newLoser)  //triggers a new pick fight
        drLogD("Ratings", "AFTER: $newWinner v $newLoser")
    }

    private fun dropName(name: Name) {
        itemListModel.updateNames(this, name.copy(isDropped = true))
    }

    @SuppressLint("SetTextI18n")
    private fun viewForName(name: Name, forceNew: Boolean = false): View =
        contestViewCache[name]?.let {
            if (forceNew) {
                contestViewCache.remove(name)
                null
            } else {
                it
            }
        } ?: (if (unboundViewCache.isNotEmpty()) {
            unboundViewCache.pop().also {
                contestViewCache[name] = it
            }
        } else {
            val inflater = LayoutInflater.from(context)
            inflater.inflate(R.layout.view_contest_entry, view as? ViewGroup, false).also {
                val viewHolder = ContestViewHolder(it)
                it.setTag(R.id.tag_viewholder, viewHolder)
                viewHolder.pickButton.also { pickButton ->
                    pickButton.setOnClickListener(this)
                    pickButton.setTag(R.id.tag_viewholder, viewHolder)
                }
                viewHolder.dropButton.also { dropButton ->
                    dropButton.setOnClickListener(this)
                    dropButton.setTag(R.id.tag_viewholder, viewHolder)
                }
                it.id = View.generateViewId()
                contestViewCache[name] = it
            }
        }.bind(name))

    private fun resetNameViewEndAction(v: View): Runnable = Runnable {
        (v.parent as ViewGroup).removeView(v)
        unboundViewCache.add(v)
        v.translationZ = 0f
        v.translationX = 0f
        v.translationY = 0f
        v.scaleX = 0f
        v.scaleY = 0f
        v.alpha = 1f
        v.rotation = 0f
    }

    private fun contestPrefix(preferences: SharedPreferences) =
        preferences.getString(getString(R.string.preferenceContestPrefix), "").let {
            if (it.isNullOrEmpty()) {
                ""
            } else {
                it.trim() + " "
            }
        }

    private fun contestPostfix(preferences: SharedPreferences) =
        preferences.getString(getString(R.string.preferenceContestPostfix), "").let {
            if (it.isNullOrEmpty()) {
                ""
            } else {
                " " + it.trim()
            }
        }

    @SuppressLint("SetTextI18n")
    private fun View.bind(name: Name): View {
        (this.getTag(R.id.tag_viewholder) as? ContestViewHolder)?.let { viewHolder ->
            val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
            viewHolder.pickButton.setTag(R.id.tag_name, name)
            viewHolder.dropButton.setTag(R.id.tag_name, name)
            viewHolder.nameTextView.text =
                "${contestPrefix(preferences)}${name.itemName}${contestPostfix(preferences)}"
            viewHolder.rootView.setTag(R.id.tag_name, name)
        }
        return this
    }
}

private class ContestViewHolder(v: View) {
    val rootView = v
    val pickButton: FrameLayout = v.findViewById(R.id.pickButton)
    val dropButton: Button = v.findViewById(R.id.dropButton)
    val nameTextView: TextView = v.findViewById(R.id.nameTextView)
}
