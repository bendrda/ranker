package me.drda.ben.ranker

import android.content.Context
import androidx.room.*
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase


@Database(entities = [Name::class, NameHeader::class], version = 3, exportSchema = false)
abstract class DrdaDatabase : RoomDatabase() {

    abstract val nameDao: NameDao

    abstract val nameHeaderDao: NameHeaderDao

    abstract val itemListDao: ItemListDao

    object Lock

    fun rebuildDatabase() {
        clearAllTables()
    }

    fun addList(headerName: String, names: Array<String>) {
        insertHeader(this, headerName, names)
    }

    companion object {

        @Volatile
        private var db: DrdaDatabase? = null

        fun database(context: Context): DrdaDatabase =
            db ?: synchronized(Lock) {
                db ?: buildDatabase(context).also { db = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, DrdaDatabase::class.java, "DrdaDatabase")
                .addMigrations(object : Migration(2, 3) {
                    override fun migrate(database: SupportSQLiteDatabase) {
                        database.execSQL("ALTER TABLE Name ADD COLUMN isDropped INTEGER NOT NULL DEFAULT 0")
                    }
                })
                .build()

        private fun insertHeader(db: DrdaDatabase, headerName: String, names: Array<String>) {
            val headerId = db.nameHeaderDao.addHeader(NameHeader(headerName))
            db.nameDao.addNames(*names.map {
                Name(it, headerId = headerId.toInt())
            }.toTypedArray())
        }
    }

}

@Dao
interface NameDao {

    @Insert
    fun addNames(vararg names: Name): LongArray

    @Delete
    fun deleteNames(vararg names: Name)

    @Update
    fun updateNames(vararg names: Name)

    @Query("SELECT * FROM Name WHERE dbId = :dbId")
    fun getName(dbId: Int): Name?

}

@Dao
interface NameHeaderDao {

    @Insert
    fun addHeaders(vararg headers: NameHeader): LongArray

    @Insert
    fun addHeader(header: NameHeader): Long

    @Delete
    fun deleteHeader(vararg headers: NameHeader)

    @Update
    fun updateHeaders(vararg headers: NameHeader)

    @Query("SELECT * FROM NameHeader")
    fun getHeaderNames(): List<NameHeader>

}

@Dao
interface ItemListDao {


    @Query("SELECT * FROM NameHeader WHERE NameHeader.dbId = :headerId")
    @Transaction
    fun getItemList(headerId: Int): ItemList


}
