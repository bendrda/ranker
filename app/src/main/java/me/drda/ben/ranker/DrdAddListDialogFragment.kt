package me.drda.ben.ranker

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Checkable
import android.widget.CheckedTextView
import androidx.annotation.IntRange
import androidx.annotation.Keep
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import java.util.zip.GZIPInputStream
import kotlin.coroutines.CoroutineContext

class DrdAddListDialogFragment : DialogFragment(), CoroutineScope {

    private val job: Job = Job()
    private var seeds: List<Seed>? = null
    private val namesInSeeds: Set<String>
        get() = seeds?.fold(mutableSetOf<String>()) { set, seed ->
            if (seed.selected) {
                set.addAll(seed.names.map { it.name })
            }
            set
        }?.toSet() ?: emptySet()
    private val seedListAdapter = SeedListAdapter()
    private val loadingProgressBar: ContentLoadingProgressBar?
        get() = dialog?.findViewById(R.id.loading) ?: view?.findViewById(R.id.loading)
    private val seedsRecyclerView: RecyclerView?
        get() = dialog?.findViewById(R.id.seedsRecyclerView)
            ?: view?.findViewById(R.id.seedsRecyclerView)
    private val headersModel: HeadersModel by activityViewModels()
    private val args by navArgs<DrdAddListDialogFragmentArgs>()
    private val defaultHeaderName by lazy {
        args.defaultHeaderName
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadSeeds()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return createView(inflater, container)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = createView(LayoutInflater.from(requireContext()), null)
        return AlertDialog.Builder(requireContext()).setView(view)
            .setNeutralButton(android.R.string.cancel, null)
            .setPositiveButton(getString(R.string.create, "0"), null)
            .create()
    }

    override fun onResume() {
        super.onResume()
        (dialog as? AlertDialog)?.getButton(Dialog.BUTTON_POSITIVE)?.setOnClickListener {
            createList()
        }
    }

    private fun createView(inflater: LayoutInflater, container: ViewGroup?): View {
        val v = inflater.inflate(R.layout.fragment_create_list, container, false)
        val recyclerView = v.findViewById<RecyclerView>(R.id.seedsRecyclerView)
        recyclerView.adapter = seedListAdapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        seedListAdapter.setOnSeedClickedListener { viewHolder ->
            val position = recyclerView.getChildAdapterPosition(viewHolder.itemView)
            val seed = seedListAdapter.currentList[position]
            seed.selected = !seed.selected
            if (viewHolder.itemView is Checkable) {
                viewHolder.itemView.isChecked = seed.selected
            }
            val count =
                namesInSeeds.count().toString()
            (dialog as? AlertDialog)?.getButton(Dialog.BUTTON_POSITIVE)?.text =
                getString(R.string.create, count)
        }
        return v
    }

    private fun loadSeeds() {
        launch {
            loadSeedsIO()
            showSeeds()
        }
    }

    private suspend fun loadSeedsIO() =
        withContext(Dispatchers.IO) {
            context?.let { context ->
                val gson = GsonBuilder()
                    .create()
                seeds = gson.fromJson(
                    GZIPInputStream(
                        context.resources.openRawResource(R.raw.names_json).buffered()
                    ).reader(), Array<Seed>::class.java
                ).toList().sorted()
            }
        }

    private fun showSeeds() {
        loadingProgressBar?.hide()
        seedListAdapter.submitList(seeds)
    }

    private fun createList() {
        loadingProgressBar?.show()
        seedsRecyclerView?.visibility = View.INVISIBLE
        headersModel.add(defaultHeaderName, namesInSeeds.toList()) {
            dismiss()
        }
    }

}

private class SeedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

private object SeedDiffItemCallback : DiffUtil.ItemCallback<Seed>() {
    override fun areItemsTheSame(oldItem: Seed, newItem: Seed): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: Seed, newItem: Seed): Boolean =
        oldItem == newItem && oldItem.selected == newItem.selected
}

private class SeedListAdapter : ListAdapter<Seed, SeedViewHolder>(SeedDiffItemCallback) {

    var listener: ((SeedViewHolder) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeedViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.view_seed, parent, false)
        val vh = SeedViewHolder(itemView)
        itemView.setOnClickListener {
            listener?.invoke(vh)
        }
        return vh
    }

    override fun onBindViewHolder(holder: SeedViewHolder, position: Int) {
        if (holder.itemView is CheckedTextView) {
            holder.itemView.text = currentList[position].description(holder.itemView.context)
            holder.itemView.isChecked = currentList[position].selected
        }
    }

    fun setOnSeedClickedListener(listener: (SeedViewHolder) -> Unit) {
        this.listener = listener
    }
}

@Keep
private data class Seed(
    @Keep @IntRange(from = 1880, to = 2999) val year: Int = 2999,
    @Keep val gender: String = "F",
    @Keep val names: List<SeedEntry> = emptyList()
) : Comparable<Seed> {

    var selected = false

    fun description(context: Context): String =
        if (gender == "M") {
            context.getString(R.string.boy_seed, year)
        } else {
            context.getString(R.string.girl_seed, year)
        }


    override fun compareTo(other: Seed): Int =
        (-1 * year.compareTo(other.year)).let {
            if (it == 0) {
                gender.compareTo(other.gender)
            } else {
                it
            }
        }
}

private data class SeedEntry(
    @Keep
    val name: String = "",
    @Keep
    val pop: Int = 0
)