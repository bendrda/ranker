package me.drda.ben.ranker

import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.*
import androidx.room.ForeignKey.Companion.CASCADE
import de.gesundkrank.jskills.GameInfo
import de.gesundkrank.jskills.IPlayer
import de.gesundkrank.jskills.ITeam
import de.gesundkrank.jskills.Rating
import java.util.*
import kotlin.math.abs

const val GAMEINFO_MEAN = 100.0
const val GAMEINFO_STDDEV = GAMEINFO_MEAN / 3.0
const val GAMEINFO_BETA = GAMEINFO_MEAN / 6.0
const val GAMEINFO_DYNAMIC = GAMEINFO_MEAN / 300.0
const val GAMEINFO_DRAW = 0.0
const val JSON_LIST = "l"
val GAME_INFO =
    GameInfo(GAMEINFO_MEAN, GAMEINFO_STDDEV, GAMEINFO_BETA, GAMEINFO_DYNAMIC, GAMEINFO_DRAW)


@Entity
data class NameHeader(
    val headerName: String,
    @PrimaryKey(autoGenerate = true)
    val dbId: Int = 0
)


data class ItemList(
    @Embedded
    val header: NameHeader
) {

    @Relation(parentColumn = "dbId", entityColumn = "headerId")
    var list: List<Name> = emptyList()
        set(newList) {
            field = newList
            _sortedList = null
        }

    @Ignore
    private var _sortedList: List<Name>? = null

    val sortedList: List<Name>
        get() {
            return _sortedList ?: fun(): List<Name> {
                val finalList = mutableListOf<Name>()
                finalList.addAll(list.sortedWith { o1, o2 ->
                    if (o1.isDropped != o2.isDropped) {
                        if (o1.isDropped) {
                            1
                        } else {
                            -1
                        }
                    } else if (o1.isDropped) {
                        o1.itemName.compareTo(o2.itemName)
                    } else if (o1.rating.mean != o2.rating.mean) {
                        o1.rating.mean.compareTo(o2.rating.mean) * -1
                    } else {
                        o1.itemName.compareTo(o2.itemName)
                    }
                })
                return finalList.toList()
            }.invoke().also { _sortedList = it }

        }


    fun findBestContestant(): Name =
        list.maxBy {
            if (it.isDropped) {
                it.rating.variance * it.rating.mean - 1e9
            } else {
                it.rating.variance * it.rating.mean
            }
        }

    fun findClosest(toName: Name): Name {
        if (list.count() < 2) {
            throw IllegalStateException("Must have at least two items in the list.")
        }
        return list.minBy {
            when {
                it == toName -> Double.MAX_VALUE
                it.isDropped -> (abs(it.rating.mean - (toName.rating.mean - 1e9)) + 1.0)
                else -> (abs(it.rating.mean - toName.rating.mean) + 1.0)
            }
        }
    }

    fun write(writer: JsonWriter) {
        writer.beginObject()
        writer.name(JSON_NAME).value(header.headerName)
            .name(JSON_LIST).beginArray()
        list.forEach {
            it.write(writer)
        }
        writer.endArray()
        writer.endObject()
    }

    companion object {
//        private val randomFudgeFactor get() = ((Math.random() / 1.95) + 0.95)
//        fun read(reader: JsonReader): ItemList? {
//            try {
//                var itemName: String? = null
//                var list: MutableList<Name> = mutableListOf()
//                reader.beginObject()
//                while (reader.hasNext()) {
//                    when(reader.nextName()) {
//                        JSON_NAME -> itemName = reader.nextString()
//                        JSON_LIST -> {
//                            reader.beginArray()
//                            while (reader.hasNext()) {
//                                Name.read(reader)?.let {
//                                    list.add(it)
//                                }
//                            }
//                            reader.endArray()
//                        }
//                    }
//                }
//                reader.endObject()
//
//                return if (itemName != null && list.isNotEmpty()) {
//                    ItemList(itemName, list)
//                } else {
//                    null
//                }
//
//            } catch (e: IOException) {
//                return null
//            }
//        }
    }
}


data class NameKey(val key: String) : IPlayer

//val Rating.stars: Int?
//    get() {
//        return if (standardDeviation < GAME_INFO.initialStandardDeviation * 2.0 / 3.0) {
//            when {
//                mean < GAME_INFO.initialMean - GAME_INFO.initialStandardDeviation * 0.440 -> 1
//                mean < GAME_INFO.initialMean + GAME_INFO.initialStandardDeviation * 0.440 -> 2
//                else -> 3
//            }
//        } else {
//            null
//        }
//    }

const val JSON_NAME = "n"
const val JSON_RATING = "r"

@Entity(
    foreignKeys = [ForeignKey(
        childColumns = ["headerId"],
        entity = NameHeader::class,
        parentColumns = ["dbId"],
        onDelete = CASCADE,
        onUpdate = CASCADE
    )]
)
@TypeConverters(value = [DBConverter::class])
data class Name(
    val itemName: String,
    val rating: Rating = GAME_INFO.defaultRating,
    @PrimaryKey(autoGenerate = true)
    val dbId: Int = 0,
    @ColumnInfo(index = true)
    val headerId: Int = 0,
    val isDropped: Boolean = false
) {
    val score: Double
        get() = rating.mean - GAME_INFO.initialMean

    @Ignore
    val player: IPlayer = NameKey(itemName)

    @Ignore
    val team: ITeam = object : ITeam {

        override val size: Int = 1

        override fun containsKey(key: IPlayer?) = key == this@Name.player

        override fun containsValue(value: Rating?) = rating == value

        override fun get(key: IPlayer?): Rating? = if (this@Name.player == key) {
            rating
        } else {
            null
        }

        override fun isEmpty(): Boolean = false

        override val entries: MutableSet<MutableMap.MutableEntry<IPlayer, Rating>>
            get() {
                val entry: MutableMap.MutableEntry<IPlayer, Rating> =
                    AbstractMap.SimpleImmutableEntry(this@Name.player, rating)
                return Collections.singleton(entry)
            }

        override val keys: MutableSet<IPlayer> =
            Collections.singleton(this@Name.player)

        override val values: MutableCollection<Rating>
            get() = Collections.singleton(rating)


        override fun clear() {
            throw UnsupportedOperationException()
        }

        override fun put(key: IPlayer?, value: Rating?): Rating? {
            throw UnsupportedOperationException()
        }

        override fun putAll(from: Map<out IPlayer, Rating>) {
            throw UnsupportedOperationException()
        }

        override fun remove(key: IPlayer?): Rating? {
            throw UnsupportedOperationException()
        }
    }

    fun write(writer: JsonWriter) {
        writer.beginObject()
            .name(JSON_NAME).value(itemName)
            .name(JSON_RATING)
        rating.write(writer)
        writer.endObject()
    }

    companion object {
        fun read(reader: JsonReader): Name? {
            var name: String? = null
            var rating: Rating? = null
            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    JSON_NAME -> name = reader.nextString()
                    JSON_RATING -> rating = readRating(reader)
                }
            }
            reader.endObject()
            return if (name != null && rating != null) {
                Name(name, rating)
            } else {
                null
            }
        }
    }

}

const val JSON_MEAN = "m"
const val JSON_STDDEV = "s"
const val JSON_CONSERV = "c"

private fun Rating.write(writer: JsonWriter) {
    writer.beginObject()
        .name(JSON_MEAN).value(mean)
        .name(JSON_STDDEV).value(standardDeviation)
        .name(JSON_CONSERV).value(conservativeStandardDeviationMultiplier)
        .endObject()
}

private fun readRating(reader: JsonReader): Rating? {
    var mean: Double? = null
    var stddev: Double? = null
    var conservativeStdDev: Double? = null
    reader.beginObject()
    while (reader.hasNext()) {
        when (reader.nextName()) {
            JSON_MEAN -> mean = reader.nextDouble()
            JSON_STDDEV -> stddev = reader.nextDouble()
            JSON_CONSERV -> conservativeStdDev = reader.nextDouble()
        }
    }
    reader.endObject()

    return if (mean != null && stddev != null && conservativeStdDev != null) {
        Rating(mean, stddev, conservativeStdDev)
    } else {
        null
    }
}

const val DELIMITER_PIPE = "|"

class DBConverter {


    @TypeConverter
    fun stringFromRating(rating: Rating): String {
        return "${rating.mean}$DELIMITER_PIPE${rating.standardDeviation}$DELIMITER_PIPE${rating.conservativeRating}"
    }

    @TypeConverter
    fun ratingFromString(string: String): Rating {
        val doubles = string.split(DELIMITER_PIPE)
        return Rating(doubles[0].toDouble(), doubles[1].toDouble(), doubles[2].toDouble())
    }
}

