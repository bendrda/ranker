package me.drda.ben.ranker

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.Keep
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton
import de.gesundkrank.jskills.Rating
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.lang.reflect.InvocationTargetException
import java.util.*
import kotlin.math.log2


class NameFragment : DrdaFragment(), View.OnClickListener {

    interface IListener {
        fun didChangeName(name: Name)
    }

    private var nameViewModel: NameViewModel? = null
    private val liveName: LiveData<Name>?
        get() = nameViewModel?.name
    private var nameCache: Name? = null
    private var listener: IListener? = null
    private var didDeleteName = false


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IListener) {
            listener = context
        } else {
            throw ClassCastException("${context.javaClass.name} must implement ${IListener::class.java.name}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }



    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_name, container, false)
        val textScore = v.findViewById<TextView>(R.id.textMean)
        val textStddev = v.findViewById<TextView>(R.id.textStddev)
        val textName = v.findViewById<TextView>(R.id.textName)
        val buttonDrop = v.findViewById<MaterialButton>(R.id.buttonDrop)
//        val buttonDelete = v.findViewById<MaterialButton>(R.id.buttonDelete)

        nameViewModel = ViewModelProvider(
            this,
            IdViewModelFactory(
                requireActivity().application,
                NameFragmentArgs.fromBundle(arguments ?: Bundle.EMPTY).nameId
            )
        ).get(NameViewModel::class.java)
        liveName!!.observe(viewLifecycleOwner, Observer { name ->
            if (nameCache != null && nameCache != name) {
                listener?.didChangeName(name ?: nameCache!!)
            }
            if (didDeleteName) {
                findNavController().navigateUp()
            }
            nameCache = name
            if (name != null) {
                textName.text = name.itemName
                textScore.text = "%.2f".format(Locale.US, name.score)
                textStddev.text = "%.2f".format(Locale.US, name.rating.certainty)
                buttonDrop.visibility = View.VISIBLE
                if (name.isDropped) {
                    buttonDrop.text = getString(R.string.restore)
                    buttonDrop.setIconResource(R.drawable.ic_arrow_upward_black_24dp)
                } else {
                    buttonDrop.text = getString(R.string.drop)
                    buttonDrop.setIconResource(R.drawable.ic_arrow_downward_black_24dp)
                }
            }
        })

        buttonDrop.setOnClickListener(this)
//        buttonDelete.setOnClickListener(this)

        return v
    }

    override fun onClick(v: View) {
        liveName?.value?.let { name ->

            when (v.id) {
                R.id.buttonDrop -> {
                    nameViewModel?.dropName(v.context, !name.isDropped)
                }
//                R.id.buttonDelete -> {
//                    AlertDialog.Builder(v.context)
//                        .setMessage("Are you sure you want to delete ${name.itemName}?")
//                        .setNegativeButton("DELETE") { _, _ ->
//                            didDeleteName = true
//                            nameViewModel?.deleteName(v.context)
//                        }.setNeutralButton("NEVER MIND", null)
//                        .create().show()
//                }

                else -> {
                }
            }
        }

    }
}

@Keep
class NameViewModel(application: Application, dbId: Int) : IdViewModel(application, dbId) {

    private val _name: MutableLiveData<Name> = MutableLiveData()
    private val dbScope = viewModelScope + Dispatchers.IO
    val name: LiveData<Name> = _name

    init {
        dbScope.launch {
            _name.postValue(
                DrdaDatabase.database(application.applicationContext).nameDao.getName(
                    dbId
                )
            )
        }
    }

    fun dropName(context: Context, isDropped: Boolean) {
        name.value?.let { name ->
            if (name.isDropped != isDropped) {
                val newName = name.copy(isDropped = isDropped)
                _name.value = newName
                dbScope.launch {
                    DrdaDatabase.database(context.applicationContext).nameDao.updateNames(newName)
                }
            }
        }
    }
}

@Keep
open class IdViewModel(application: Application, @Suppress("UNUSED_PARAMETER") dbId: Int) :
    AndroidViewModel(application)

class IdViewModelFactory(private val application: Application, private val dbId: Int) :
    ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (IdViewModel::class.java.isAssignableFrom(modelClass)) {
            try {
                return modelClass.getConstructor(Application::class.java, Int::class.java)
                    .newInstance(application, dbId)
            } catch (e: NoSuchMethodException) {
                throw  RuntimeException("Cannot create an instance of $modelClass", e)
            } catch (e: IllegalAccessException) {
                throw  RuntimeException("Cannot create an instance of $modelClass", e)
            } catch (e: InstantiationException) {
                throw  RuntimeException("Cannot create an instance of $modelClass", e)
            } catch (e: InvocationTargetException) {
                throw  RuntimeException("Cannot create an instance of $modelClass", e)
            }
        }
        return super.create(modelClass)
    }

}

private val Rating.certainty: Double
    get() = log2(GAME_INFO.initialStandardDeviation / standardDeviation)