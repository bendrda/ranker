package me.drda.ben.ranker

import android.app.Application
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import kotlinx.coroutines.*
import kotlin.collections.set
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), NameFragment.IListener, CoroutineScope {

    private val job: Job = Job()
    private val itemListModel by viewModels<ItemListModel>()
    private val titleModel by viewModels<TitleModel>()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        titleModel.title.observe(this, Observer { title = it ?: getString(R.string.app_name) })
        setContentView(R.layout.activity_main)
    }


    override fun didChangeName(name: Name) {
        itemListModel.updateNames(this, name)
    }

    //    private fun load(file: File): MutableList<NameHeader> {
//        val r = JsonReader(file.reader())
//        val lists: MutableList<NameHeader> = mutableListOf()
//        r.use { reader ->
//            reader.beginObject()
//            while(reader.hasNext()) {
//                when (reader.nextName()) {
//                    JSON_VERSION -> reader.skipValue()
//                    JSON_LISTS -> {
//                        reader.beginArray()
//                        while (reader.hasNext()) {
//                            NameHeader.read(reader)?.let { list ->
//                                lists.add(list)
//                            }
//                        }
//                        reader.endArray()
//                    }
//                }
//            }
//        }
//        return lists
//    }


    companion object {

//        fun save(appContext: Context, lists: List<NameHeader>) {
//            save(file(appContext), lists)
//        }
//
//        private fun save(file: File, lists: List<NameHeader>) {
//            val w = JsonWriter(file.writer())
//            w.use { writer ->
//                writer.beginObject()
//                writer.itemName(JSON_VERSION).value(1)
//                    .itemName(JSON_LISTS).beginArray()
//                lists.forEach {
//                    it.write(writer)
//                }
//                writer.endArray()
//                writer.endObject()
//            }
//        }
//
//        private fun file(appContext: Context) = File(appContext.applicationContext.filesDir, "lists.json")
    }


}

class TitleModel : ViewModel() {
    val title = MutableLiveData<CharSequence>()
}

class ItemListModel(application: Application) : AndroidViewModel(application) {
    private val _itemLists = mutableMapOf<Int, MutableLiveData<ItemList>>()
    private val dbScope = viewModelScope + Dispatchers.IO
    fun getItemList(dbId: Int): LiveData<ItemList> {
        if (_itemLists[dbId] == null) {
            _itemLists[dbId] = MutableLiveData()
            getListFromDb(dbId)
        }
        return _itemLists[dbId]!!
    }

    private fun getListFromDb(dbId: Int) {
        dbScope.launch {
            val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
            val itemList = db.itemListDao.getItemList(dbId)
            _itemLists[dbId]?.postValue(itemList)
        }
    }

    fun addName(itemListId: Int, nameString: String) {
        val itemList = _itemLists[itemListId]?.value
        if (itemList != null) {
            if (itemList.list.find { nameString == it.itemName } != null) {
                throw IllegalStateException("nameString $nameString already exists!")
            }
            val name = Name(nameString, headerId = itemList.header.dbId)
            dbScope.launch {
                val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
                val id = db.nameDao.addNames(name).firstOrNull()?.toInt()
                if (id != null) {
                    _itemLists[itemListId]?.apply {
                        value?.let { oldItemList ->
                            val newItemList = oldItemList.copy()
                            newItemList.list = oldItemList.list.plus(name.copy(dbId = id))
                            postValue(newItemList)
                        }
                    }
                }
            }
        }
    }

    fun updateNames(scope: CoroutineScope, vararg names: Name) {
        scope.launch {
            updateNamesIO(*names)
            _itemLists[names.first().headerId]?.apply {
                value?.let { oldItemList ->
                    val newItemList = oldItemList.copy()
                    newItemList.list = oldItemList.list.map {
                        names.find { name ->
                            name.dbId == it.dbId
                        } ?: it
                    }
                    value = newItemList
                }
            }
        }
    }

    private suspend fun updateNamesIO(vararg names: Name) = withContext(Dispatchers.IO) {
        val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
        db.nameDao.updateNames(*names)
    }

    fun updateHeader(
        itemListId: Int,
        nameHeader: NameHeader
    ) {
        _itemLists[itemListId]?.value?.let { itemList ->
            _itemLists[itemListId]?.value =
                itemList.copy(header = nameHeader).also {
                    it.list = itemList.list
                }
        }
    }

    fun deleteNames(coroutineScope: CoroutineScope, itemListId: Int) {
        getItemList(itemListId).value?.let { itemList ->
            coroutineScope.launch {
                deleteNamesIO(*itemList.list.toTypedArray())
            }
        }
    }

    private suspend fun deleteNamesIO(vararg names: Name) = withContext(Dispatchers.IO) {
        val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
        db.nameDao.deleteNames(*names)
    }
}

const val JSON_LISTS = "l"
const val JSON_VERSION = "v"