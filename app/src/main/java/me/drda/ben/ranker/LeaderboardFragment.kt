package me.drda.ben.ranker

import android.graphics.PorterDuff
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext
import kotlin.math.roundToInt

fun Int?.compareTo(other: Int?): Int = (this ?: -1).compareTo(other ?: -1)

class LeaderboardFragment : DrdaFragment(), OnNameSelectedListener, SearchView.OnQueryTextListener,
    CoroutineScope {

    private val job: Job = Job()
    private lateinit var adapter: NameAdapter
    private var searchTerm: String? = null
    private var itemListCache: ItemList? = null
    private val listsModel by activityViewModels<ItemListModel>()
    private val args by navArgs<LeaderboardFragmentArgs>()
    private val itemListId by lazy {
        args.itemListId
    }
    private val headersModel by activityViewModels<HeadersModel>()
    private val titleModel by activityViewModels<TitleModel>()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private val displayList: List<Name>?
        get() = if (searchTerm?.isNotEmpty() == true) {
            itemListCache?.sortedList?.filter {
                it.itemName.startsWith(
                    searchTerm ?: "",
                    ignoreCase = true
                )
            }
        } else {
            itemListCache?.sortedList
        }

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_leaderboard, container, false)
        val fab = v.findViewById<FloatingActionButton>(R.id.fabContest)
        val recyclerView = v.findViewById<RecyclerView>(R.id.recyclerViewLeader)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                inflater.context,
                LinearLayoutManager.VERTICAL
            )
        )
        fab.setOnClickListener(this::toContest)

        adapter = NameAdapter(this)
        recyclerView.adapter = adapter
        listsModel.getItemList(itemListId).observe(viewLifecycleOwner) { itemList ->
            fab.isEnabled = itemList.list.size > 1
            itemListCache = itemList
            adapter.submitList(displayList)
            titleModel.title.value = itemList.header.headerName
        }
        return v
    }

    override fun onStart() {
        super.onStart()
        itemListCache?.header?.headerName?.let {
            titleModel.title.value = it
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        if (menu.findItem(R.id.itemAddName) == null) {
            inflater.inflate(R.menu.leaderboard, menu)
            (menu.findItem(R.id.itemSearch).actionView as SearchView).apply {
                this.isSubmitButtonEnabled = false
                this.isQueryRefinementEnabled = false
                this.setOnQueryTextListener(this@LeaderboardFragment)
                inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME.or(InputType.TYPE_CLASS_TEXT)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.itemAddName -> {
                context?.let { context ->
                    val builder = AlertDialog.Builder(context)
                    val editText = EditText(context)
                    builder.setView(editText)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            addName(editText.text.toString())
                        }
                        .setNeutralButton(android.R.string.cancel) { _, _ ->
                            //do nothing
                        }
                    builder.create().show()
                }
                true
            }
            R.id.itemRename -> {
                context?.let { context ->
                    val builder = AlertDialog.Builder(context)
                    val editText = EditText(context).also {
                        it.hint = itemListCache?.header?.headerName
                    }
                    builder.setView(editText)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            editText.text.takeIf { it.isNotBlank() }?.toString()?.let {
                                renameList(it)
                            }
                        }
                        .setNeutralButton(android.R.string.cancel, null)
                        .create().show()
                }
                true
            }
            R.id.itemDelete -> {
                context?.let { context ->
                    AlertDialog.Builder(context)
                        .setMessage(R.string.delete_message)
                        .setTitle(
                            getString(
                                R.string.delete_list_button,
                                itemListCache?.header?.headerName
                            )
                        )
                        .setNegativeButton(R.string.delete) { _, _ ->
                            deleteList()
                        }
                        .setNeutralButton(android.R.string.cancel, null)
                        .create().show()

                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun renameList(newName: String) {
        val newHeader = headersModel.rename(this, itemListId, newName)
        listsModel.updateHeader(itemListId, newHeader)
    }

    private fun deleteList() {
        headersModel.delete(this, itemListId)
        listsModel.deleteNames(this, itemListId)
        findNavController().navigateUp()
    }

    override fun onNameSelected(v: View, adapterPosition: Int) {
        adapter.getItemAt(adapterPosition)?.let { name ->
            try {
                v.findNavController()
                    .navigate(
                        LeaderboardFragmentDirections.actionLeaderboardFragmentToNameFragment(
                            name.dbId
                        )
                    )
            } catch (ignore: IllegalArgumentException) {

            }
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean = true

    override fun onQueryTextChange(newText: String?): Boolean {
        searchTerm = newText
        adapter.submitList(displayList)
        return true
    }


    private fun addName(name: String) {
        val trimmedName = name.trim()
        val existingName = itemListCache?.sortedList?.find { it.itemName.equals(trimmedName, true) }
        if (existingName != null) {
            context?.let {
                AlertDialog.Builder(it)
                    .setMessage(getString(R.string.name_already_exists, existingName.itemName))
                    .setNeutralButton(android.R.string.ok, null)
                    .create().show()
            }
        } else {
            listsModel.addName(itemListId, trimmedName)
        }
    }

    private fun toContest(v: View) {
        val itemListId = LeaderboardFragmentArgs.fromBundle(arguments ?: Bundle.EMPTY).itemListId
        val action =
            LeaderboardFragmentDirections.actionLeaderboardFragmentToContestFragment(itemListId)
        v.findNavController().navigate(action)
    }

}

interface OnNameSelectedListener {
    fun onNameSelected(v: View, adapterPosition: Int)

}

class NameAdapter(private val onNameSelectedListener: OnNameSelectedListener) :
    ListAdapter<Name, NameViewHolder>(object : DiffUtil.ItemCallback<Name>() {
        override fun areItemsTheSame(oldItem: Name, newItem: Name): Boolean =
            oldItem.dbId == newItem.dbId

        override fun areContentsTheSame(oldItem: Name, newItem: Name): Boolean = oldItem == newItem
    }) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NameViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_name, parent, false)
        v.background.mutate()
        return NameViewHolder(v, onNameSelectedListener)
    }

    override fun onBindViewHolder(holder: NameViewHolder, position: Int) {
        val name = getItem(position)
        holder.textName.text = name.itemName
        if (name.isDropped) {
            holder.textScore.text = "--"
            holder.itemView.background.setTintMode(PorterDuff.Mode.MULTIPLY)
            holder.itemView.background.setTint(0x7feeeeee)
        } else {
            holder.textScore.text = name.score.roundToInt().toString()
            holder.itemView.background.setTintList(null)
        }

    }

    fun getItemAt(position: Int): Name? = if (position < itemCount && position > -1) {
        getItem(position)
    } else {
        null
    }
}

class NameViewHolder(itemView: View, private val onNameSelectedListener: OnNameSelectedListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener {
    val textName: TextView = itemView.findViewById(R.id.textName)
    val textScore: TextView = itemView.findViewById(R.id.textScore)

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        onNameSelectedListener.onNameSelected(v, adapterPosition)
    }
}
