package me.drda.ben.ranker

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat


class DrdaPreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}