package me.drda.ben.ranker

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.*


class ListsFragment : DrdaFragment(), SelectionListener {

    private lateinit var adapter: ListsListAdapter
    private val headersModel: HeadersModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_lists, container, false)

        val recyclerView = v.findViewById<RecyclerView>(R.id.recyclerViewLists)
        val fabAddList = v.findViewById<FloatingActionButton>(R.id.fabAddList)
        val textWomp = v.findViewById<TextView>(R.id.textWomp)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                inflater.context,
                LinearLayoutManager.VERTICAL
            )
        )
        adapter = ListsListAdapter(this)
        headersModel.lists.observe(requireActivity()) {
            adapter.submitList(it)
            if (it.isEmpty()) {
                textWomp.visibility = View.VISIBLE
            } else {
                textWomp.visibility = View.GONE
            }
        }
        recyclerView.adapter = adapter
        fabAddList.setOnClickListener {
            showAddListDialog(it)
        }
        return v
    }

    override fun onStart() {
        super.onStart()
        activityViewModels<TitleModel>().value.title.value = null
    }

    override fun onItemClick(item: NameHeader, view: View) {
        val action = ListsFragmentDirections.actionListsFragmentToLeaderboardFragment(item.dbId)
        view.findNavController().navigate(action)
    }

    private fun showAddListDialog(view: View) {
        val newName = newHeaderName(view.context, headersModel.lists.value ?: emptyList())
        view.findNavController()
            .navigate(ListsFragmentDirections.actionListsFragmentToDrdAddListDialogFragment(newName))
    }

    private fun newHeaderName(context: Context, headers: List<NameHeader>): String {
        var namesString = context.getString(R.string.header_name)
        var count = 0
        while (headers.find { it.headerName == namesString } != null) {
            ++count
            namesString = context.getString(R.string.header_name_count, count)
        }
        return namesString
    }
}

interface SelectionListener {
    fun onItemClick(item: NameHeader, view: View)
}

class ListsListAdapter(private val listener: SelectionListener) :
    ListAdapter<NameHeader, SimpleViewHolder>(object : DiffUtil.ItemCallback<NameHeader>() {
        override fun areItemsTheSame(oldItem: NameHeader, newItem: NameHeader): Boolean =
            oldItem.dbId == newItem.dbId

        override fun areContentsTheSame(oldItem: NameHeader, newItem: NameHeader): Boolean =
            oldItem.headerName == newItem.headerName

    }) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_list, parent, false)
        v.setOnClickListener {
            listener.onItemClick(getItem(SimpleViewHolder.fromView(it).adapterPosition), it)
        }
        return SimpleViewHolder.fromView(v)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        (holder.itemView as TextView).text = getItem(position).headerName
    }

}

class SimpleViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    companion object {
        fun fromView(v: View): SimpleViewHolder {
            val holder = v.getTag(R.id.tag_viewholder) as? SimpleViewHolder ?: SimpleViewHolder(v)
            v.setTag(R.id.tag_viewholder, holder)
            return holder
        }
    }
}

class HeadersModel(application: Application) : AndroidViewModel(application) {

    private val _headerNames: MutableLiveData<List<NameHeader>> = MutableLiveData()
    private val dbScope = viewModelScope + Dispatchers.IO
    val lists: LiveData<List<NameHeader>>
        get() = _headerNames

    init {
        dbScope.launch {
            val db = DrdaDatabase.database(application)
            val names = db.nameHeaderDao.getHeaderNames()
            _headerNames.postValue(names)
        }

    }

    fun startOver() {
        dbScope.launch {
            val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
            db.rebuildDatabase()
            val names = db.nameHeaderDao.getHeaderNames()
            _headerNames.postValue(names)
        }
    }

    fun add(s: String, sorted: List<String>, onComplete: () -> Unit) {
        val it = ProtoHeader(s, sorted)
        dbScope.launch {
            val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
            db.addList(it.header, it.names.toTypedArray())
            val names = db.nameHeaderDao.getHeaderNames()
            launch(Dispatchers.Main) {
                _headerNames.value = names
                onComplete.invoke()
            }
        }
    }

    fun rename(coroutineScope: CoroutineScope, itemListId: Int, newName: String): NameHeader {
        lists.value?.let { headers ->
            headers.indexOfFirst { it.dbId == itemListId }.takeIf { it > -1 }?.let { ndx ->
                val newHeader = headers[ndx].copy(headerName = newName)
                coroutineScope.launch {
                    updateIO(headers, ndx, newHeader)
                }
                return newHeader
            }
        }
        return NameHeader(headerName = newName)
    }

    fun delete(coroutineScope: CoroutineScope, itemListId: Int) {
        coroutineScope.launch {
            deleteIO(itemListId)
        }
    }

    private suspend fun deleteIO(itemListId: Int) = withContext(dbScope.coroutineContext) {
        val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
        lists.value?.let { headers ->
            headers.indexOfFirst { it.dbId == itemListId }.takeIf { it > -1 }?.let { ndx ->
                db.nameHeaderDao.deleteHeader(headers[ndx])
                _headerNames.postValue(headers.slice(0 until ndx) + headers.slice(ndx + 1 until headers.count()))
            }
        }
    }

    private suspend fun updateIO(
        currentHeaders: List<NameHeader>,
        ndx: Int,
        newHeader: NameHeader
    ) = withContext(dbScope.coroutineContext) {
        val db = DrdaDatabase.database(getApplication<Application>().applicationContext)
        db.nameHeaderDao.updateHeaders(newHeader)
        _headerNames.postValue(
            currentHeaders.slice(0 until ndx) + newHeader + currentHeaders.slice(
                ndx + 1 until currentHeaders.count()
            )
        )
    }
}


data class ProtoHeader(val header: String, val names: List<String>)